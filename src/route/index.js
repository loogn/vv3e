import { createRouter, createWebHistory } from 'vue-router'
import { store } from '../utils'
const routes = [
    {
        path: '/login',
        name: 'Login',
        alias: ['/'],
        component: () => import('../views/Login.vue'),
        meta: { title: '登录' }
    },
    {

        path: '/admin',
        name: 'AdminLayout',
        component: () => import('../views/AdminLayout.vue'),
        children: [
            {
                path: "",
                name: 'welcome',
                component: () => import('../views/Welcome.vue'),
                meta: { title: '首页' }
            }
            , {
                path: "a",
                name: 'A',
                component: () => import('../views/A.vue'),
                meta: { title: "页面A" }
            }
            , {
                path: "sysrole",
                component: () => import('../views/auth/sysrole.vue'),
                meta: { title: '角色管理', bcs: ['权限管理'] }
            }
            , {
                path: "product/category",
                component: () => import('../views/product/categoryManager.vue'),
                meta: { title: '商品分类', bcs: ['商品管理'] }
            }
            , {
                path: "content/newslist",
                component: () => import('../views/content/newslist.vue'),
                meta: { title: '文章列表', bcs: ['内容管理'] }
            }
            , {
                path: "content/newstype",
                component: () => import('../views/content/newstype.vue'),
                meta: { title: '文章分类', bcs: ['内容管理'] }
            }
        ]
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
})


router.beforeEach((to, from, next) => {
    if (to.path === "/login") {
        store.remove('token');
    }
    // console.log('to.path', to.path);
    var token = store.get("token");
    if (!token && to.path != "/login") {
        next({
            path: "/login"
        });
    } else {

        let bcs = [];//面包屑
        if (to.meta && to.meta.bcs) {
            bcs = bcs.concat(to.meta.bcs);
        }
        if (to.meta && to.meta.title) {
            bcs.push(to.meta.title);
            document.title = to.meta.title + ' - ' + import.meta.env.VITE_APP_TITLE;
        } else {
            document.title = import.meta.env.VITE_APP_TITLE;
        }
        store.resetBcs(bcs);
        next();
    }
});


export default router;

