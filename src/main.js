// import 'default-passive-events'
import { createApp } from 'vue'
import App from './App.vue'
const app = createApp(App)

//element
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import 'element-plus/dist/index.css'
app.use(ElementPlus, {
    locale: zhCn,
    size: "small"
})

//路由
import router from './route'
app.use(router);

//全局样式
import './assets/style.scss';
import 'bulma';


//quilleditor
import { QuillEditor } from '@vueup/vue-quill'
import '@vueup/vue-quill/dist/vue-quill.snow.css';
app.component('QuillEditor', QuillEditor)

//注册全部图标
// import regIcons from './hooks/regIcons';
// regIcons(app);

// import NameIcon from './components/NameIcon.vue';
// app.component('nicon', NameIcon)

app.mount('#app');
