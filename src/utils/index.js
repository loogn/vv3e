import request from './request'
const post = request.post;
const getHeaders=request.getHeaders;
import store from './store';

const baseUrl = import.meta.env.VITE_API_BASE_URL;
export {
    post,
    getHeaders,
    store,
    baseUrl
}